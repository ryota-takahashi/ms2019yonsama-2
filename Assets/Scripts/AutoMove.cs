﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class AutoMove : MonoBehaviour {

    UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter m_Player;

    [SerializeField]
    Vector3 m_Speed;

    [SerializeField, Range(0, 5)]
    float m_MoveLimit;

    [SerializeField, Range(0.0f, 1.0f)]
    float m_JoyconRotLimit;

    private List<Joycon> m_joycons;
    private Joycon m_joyconL;
    private Joycon m_joyconR;

    // Use this for initialization
    void Start () {
        m_Player = gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>();

        m_joycons = JoyconManager.Instance.j;

        if (m_joycons == null || m_joycons.Count <= 0) return;

        m_joyconL = m_joycons.Find(c => c.isLeft);
        m_joyconR = m_joycons.Find(c => !c.isLeft);

    }
	
	// Update is called once per frame
	void Update () {

        Vector3 moveVar = Vector3.zero;

        // ジョイコン(右)のZ軸の傾きを取得
        float fAccelZ = m_joyconR.GetAccel().z;

        fAccelZ = Mathf.Max(fAccelZ, -m_JoyconRotLimit);
        fAccelZ = Mathf.Min(fAccelZ, m_JoyconRotLimit);

        int Buf = (int )(fAccelZ * 100);

        fAccelZ = Buf * 0.01f;

        float per = (fAccelZ + m_JoyconRotLimit) / ( m_JoyconRotLimit * 2 );

        moveVar = gameObject.transform.localPosition;
        moveVar.x = -( m_MoveLimit * 2 * per - m_MoveLimit);

        gameObject.transform.localPosition = moveVar;

        // 傾き取得テスト
        // Debug.Log("傾きテスト：" + m_joyconR.GetVector().eulerAngles);
    }

    private void FixedUpdate()
    {
        // 奥に進む
        m_Player.Move(m_Speed, false, false);
    }

    public void SetSpeed(Vector3 Speed)
    {
        m_Speed = Speed;
    }
}
