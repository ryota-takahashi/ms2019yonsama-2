﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenObj : MonoBehaviour {

    [SerializeField]
    GameObject DestoryEffect;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDestroy()
    {
        Instantiate(DestoryEffect, transform.position,Quaternion.identity);
        
    }
}
