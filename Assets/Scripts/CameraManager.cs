﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    bool m_bTracePlayer;

    Quaternion Front;

    [SerializeField]
    Vector3 Offset;

    [SerializeField]
    GameObject m_Player;

    // Use this for initialization
    void Start () {
        Front = Quaternion.Euler(0, 180, 0);

    }
	
	// Update is called once per frame
	void Update () {

		if(m_bTracePlayer)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Front, 0.02f);
            transform.position = Vector3.Lerp(transform.position, m_Player.transform.position + Offset, 0.1f);
        }
	}

    public void SetTrace(bool bTrace)
    {
        m_bTracePlayer = bTrace;
    }

    public void ChangeAngle(Quaternion Angle)
    {
        Front = Angle;
    }
}
