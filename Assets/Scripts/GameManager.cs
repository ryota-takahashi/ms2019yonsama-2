﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private List<Joycon> m_joycons;
    private Joycon m_joyconL;
    private Joycon m_joyconR;

    [SerializeField]
    GameObject m_Player;

    // Use this for initialization
    void Start () {

        // ジョイコンの取得
        m_joycons = JoyconManager.Instance.j;

        if (m_joycons == null || m_joycons.Count <= 0) return;

        m_joyconL = m_joycons.Find(c => c.isLeft);
        m_joyconR = m_joycons.Find(c => !c.isLeft);

        m_Player.GetComponent<AutoMove>().enabled = true;
        m_Player.GetComponent<AutoMove>().SetSpeed(new Vector3(0, 0, -1));
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        
    }
}
