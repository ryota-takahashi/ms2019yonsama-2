﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HpBarCtrl : MonoBehaviour {

    GameObject TenpaGaugeObject;

    T_Wave script;

    Slider _slider;
    float _hp = 0.0f;


    // Use this for initialization
    void Start () {
        //スライダーを取得する
        _slider = GameObject.Find("Slider").GetComponent<Slider>();

        TenpaGaugeObject = GameObject.Find("WaveGenerator");
        script = TenpaGaugeObject.GetComponent<T_Wave>();

	}


	// Update is called once per frame
	void Update () {

        //天パワーの取得
        float TenpaPower = script.TenpaPower;

        if (TenpaPower >= 100.0f)
        {
            TenpaPower = 100.0f;
        }

        //HPゲージに値を設定
        _slider.value = TenpaPower;

	}
}
