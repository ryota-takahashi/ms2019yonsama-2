﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class T_Wave : MonoBehaviour
{
    private List<Joycon> m_joycons;
    private Joycon m_joyconL;
    private Joycon m_joyconR;

    private GameObject[] EffectsObj;

    public float TenpaPower = 100.0f;   //天パゲージ

    

    public float UseTenpaPower = 20.0f;    //ボタンを押したときに消費するゲージ量

    // Use this for initialization
    void Start()
    {
        m_joycons = JoyconManager.Instance.j;

        if (m_joycons == null || m_joycons.Count <= 0) return;

        m_joyconL = m_joycons.Find(c => c.isLeft);
        m_joyconR = m_joycons.Find(c => !c.isLeft);

        EffectsObj = GameObject.FindGameObjectsWithTag("WaveEffect");
    }

    // Update is called once per frame
    void Update()
    {
        //天パパワーの自然回復
        TenpaPower += 0.3f;

        if (TenpaPower >= 100.0f)//上限になったら値を合わせる
        {
            TenpaPower = 100.0f;
        }

        // 右ジョイコンのボタンを押したら
        //if (m_joyconR.GetButton(Joycon.Button.DPAD_RIGHT) ||
        //    m_joyconR.GetButton(Joycon.Button.DPAD_UP) ||
        //    m_joyconR.GetButton(Joycon.Button.DPAD_LEFT) ||
        //    m_joyconR.GetButton(Joycon.Button.DPAD_DOWN) ||
        //    Input.GetKeyDown(KeyCode.Space) ||
        //    Input.GetKeyDown(KeyCode.Return))
        if (    Input.GetKeyDown(KeyCode.Space) ||
                Input.GetKeyDown(KeyCode.Return))   //キーボードのスペースボタンとエンターキーを押すと入る
        {
            if (TenpaPower >= UseTenpaPower)//天パワーが消費する量より多かったら電波発射
            {
                //電波発射！！！
                Debug.Log("dennpa OK!");

                //天パワーの消費
                TenpaPower -= UseTenpaPower;
                if (TenpaPower < 0)//0より少なくなっていたら
                {
                    TenpaPower = 0.0f;
                }

                // 当たり判定ON
                gameObject.GetComponent<SphereCollider>().enabled = true;

                // ジョイコンを振動させる
                m_joyconR.SetRumble(160, 320, 0.6f, 1);

                for (int i = 0; i < EffectsObj.Length; i++)
                {
                    EffectsObj[i].SetActive(true);
                }
            }
            else
            {
                //電波発射失敗
                Debug.Log("dennpa NO!");
            }

        }
        else
        {
            // 当たり判定OFF
            gameObject.GetComponent<SphereCollider>().enabled = false;

            for(int i = 0; i < EffectsObj.Length; i++)
            {
                EffectsObj[i].SetActive(false);
            }
        }




    }

    private void OnTriggerStay(Collider other)
    {
        // 壊れるオブジェクトに当たったら
        if (other.gameObject.tag == "BrokenObj")
        {
            // とりあえず消す
            Destroy(other.gameObject);
        }
    }
}

