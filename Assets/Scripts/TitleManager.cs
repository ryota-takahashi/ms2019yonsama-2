﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleManager : MonoBehaviour {

    private List<Joycon> m_joycons;
    private Joycon m_joyconL;
    private Joycon m_joyconR;

    UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter m_Player;

    bool m_bStart;

    [SerializeField]
    GameObject LogoObj;

    [SerializeField]
    GameObject m_CheckPoint1;

    [SerializeField]
    GameObject m_CheckPoint2;

    [SerializeField]
    GameObject m_GameManager;


    // Use this for initialization
    void Start () {

        // ジョイコンの取得
        m_joycons = JoyconManager.Instance.j;

        if (m_joycons == null || m_joycons.Count <= 0) return;

        m_joyconL = m_joycons.Find(c => c.isLeft);
        m_joyconR = m_joycons.Find(c => !c.isLeft);

        // プレイヤーの取得
        m_Player = GameObject.FindGameObjectWithTag("Player").GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>();

        m_bStart = false;       
    }

    // Update is called once per frame
    void Update()
    {
        // 右ジョイコンのボタンを押したら
        if (m_joyconR.GetButton(Joycon.Button.DPAD_RIGHT)   ||
            m_joyconR.GetButton(Joycon.Button.DPAD_UP)      ||
            m_joyconR.GetButton(Joycon.Button.DPAD_LEFT)    ||
            m_joyconR.GetButton(Joycon.Button.DPAD_DOWN))
        {
            // プレイヤーが飛び込む
            MoveStart();
        }



        // プレイヤーが着地したら
        if (m_CheckPoint2.GetComponent<CheckpointOnPlayer>().GetCheck() == true)
        {
            // ゲームに切り替える
            m_GameManager.SetActive(true);

            // タイトル用のオブジェクトを消す
            Destroy(GameObject.Find("TitleObj"));

            Camera.main.GetComponent<CameraManager>().ChangeAngle(Quaternion.Euler(new Vector3(0, 180, 0)));

            gameObject.SetActive(false);
        }

    }

    void MoveStart()
    {
        m_bStart = true;

        // 「PressButton」を消す
        Destroy(GameObject.Find("PressButton"));

    }

    private void FixedUpdate()
    {

        if(m_bStart)
        {

            // プレイヤーが端に到達したら
            if (m_CheckPoint1.GetComponent<CheckpointOnPlayer>().GetCheck() == true)
            {
                // カメラ追従開始
                Camera.main.GetComponent<CameraManager>().SetTrace(true);
                Camera.main.GetComponent<CameraManager>().ChangeAngle(Quaternion.Euler(new Vector3(60, 180, 0)));

                // ジャンプ
                m_Player.Move(new Vector3(0, 0, -1), false, true);
            }else
            {
                // 奥に進む
                m_Player.Move(new Vector3(0, 0, -1), false, false);
            }

            // タイトルロゴを徐々に消す
            LogoObj.GetComponent<Image>().color -= new Color(0, 0, 0, 0.02f);
        }

    }
}
